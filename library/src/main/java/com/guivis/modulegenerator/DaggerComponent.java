package com.guivis.modulegenerator;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.SOURCE;

@Target(TYPE)
@Retention(SOURCE)
public @interface DaggerComponent {
    /*
    Module Classes to be added to component
     */
    Class<?>[] modules() default {};
}
