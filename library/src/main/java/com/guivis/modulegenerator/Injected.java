package com.guivis.modulegenerator;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.SOURCE;

@Target(TYPE)
@Retention(SOURCE)
public @interface Injected {
    /*
    Name of the module this is to be inject into.
     */
    String value() default "default";
}
