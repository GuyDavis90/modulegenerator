package com.guivis.modulegenerator;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.RetentionPolicy.SOURCE;

@Target(CONSTRUCTOR)
@Retention(SOURCE)
public @interface InjectedCtor {
}
