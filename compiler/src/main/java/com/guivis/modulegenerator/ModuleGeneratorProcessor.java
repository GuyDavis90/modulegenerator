package com.guivis.modulegenerator;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.sun.tools.javac.code.Attribute;
import com.sun.tools.javac.code.Type;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.annotation.processing.SupportedAnnotationTypes;
import javax.annotation.processing.SupportedSourceVersion;
import javax.inject.Inject;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.ElementFilter;

import dagger.Module;
import dagger.Provides;
import eu.f3rog.blade.compiler.util.ProcessorError;
import eu.f3rog.blade.compiler.util.ProcessorUtils;

@SupportedAnnotationTypes({"javax.inject.Inject", "com.guivis.modulegenerator.Injected"})
@SupportedSourceVersion(SourceVersion.RELEASE_7)
public class ModuleGeneratorProcessor extends AbstractProcessor {
    private Map<String, com.guivis.modulegenerator.ModuleBuilder> mModuleBuilderMap = new HashMap<>();

    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
        ProcessorUtils.setProcessingEnvironment(processingEnvironment);
    }

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment roundEnvironment) {
        if (set.isEmpty()) {
            return false;
        }

        Set<? extends Element> elements = roundEnvironment.getElementsAnnotatedWith(Inject.class);

        Set<? extends Element> injectedClasses = roundEnvironment.getElementsAnnotatedWith(Injected.class);

        Set<VariableElement> injectedConstructorTypes = new HashSet<>();
        for (Element p : injectedClasses) {
            TypeElement injectedType = (TypeElement) p;
            List<ExecutableElement> constructors = ElementFilter.constructorsIn(injectedType.getEnclosedElements());
            if (constructors.size() > 1) {
                System.out.println("More than one Constructor for class " + injectedType + "...");
                System.out.println("Looking for @InjectedCtor...");
                ExecutableElement injectedCtor = null;
                for (ExecutableElement constructor : constructors) {
                    if (constructor.getAnnotation(InjectedCtor.class) != null) {
                        injectedCtor = constructor;
                        break;
                    }
                }
                if (injectedCtor != null) {
                    List<? extends VariableElement> parameters = injectedCtor.getParameters();
                    injectedConstructorTypes.addAll(parameters);
                } else {
                    System.out.println("No @InjectedCtor Found! + Multiple Constructors but no InjectedCtor!");
                }
            } else {
                List<? extends VariableElement> parameters = constructors.get(0).getParameters();
                injectedConstructorTypes.addAll(parameters);
            }
        }

        Set<TypeName> processedElementTypeNames = new HashSet<>();
        Map<TypeElement, VariableElement> injectedToTypeMap = new HashMap<>();

        Set<? extends Element> otherModules = roundEnvironment.getElementsAnnotatedWith(Module.class);
        Set<VariableElement> otherModuleConstructorTypes = new HashSet<>();
        for (Element p : otherModules) {
            TypeElement moduleType = (TypeElement) p;
            List<ExecutableElement> methods = ElementFilter.methodsIn(moduleType.getEnclosedElements());
            for (ExecutableElement method : methods) {
                if (method.getAnnotation(Provides.class) == null) {
                    continue;
                }
                List<? extends VariableElement> parameters = method.getParameters();
                otherModuleConstructorTypes.addAll(parameters);
            }
        }

        List<Element> injectsAndConstructorTypes = new ArrayList<>();
        injectsAndConstructorTypes.addAll(elements);
        injectsAndConstructorTypes.addAll(injectedConstructorTypes);
        injectsAndConstructorTypes.addAll(otherModuleConstructorTypes);

        // First pass through all @Injected classes for exact mapping
        for (Element p : injectedClasses) {
            TypeElement typeElement = (TypeElement) p;
            for (Element element : injectsAndConstructorTypes) {
                Type type = ProcessorUtils.getBoundedType((VariableElement) element);
                TypeName elementType = ClassName.get(type);
                if (TypeName.get(typeElement.asType()).equals(elementType)) {
                    System.out.println("PreMapping Injected Class [" + p + "] to Injected Type[" + elementType + "]");
                    injectedToTypeMap.put(typeElement, (VariableElement) element);
                    processedElementTypeNames.add(elementType);
                    break;
                }
            }
        }

        // Second pass through any @Inject/Constructor types for subtype mapping (in case of interfaces)
        for (Element element : injectsAndConstructorTypes) {
            Type type = ProcessorUtils.getBoundedType((VariableElement) element);
            TypeName elementType = ClassName.get(type);
            if (processedElementTypeNames.contains(elementType)) {
                System.out.println("Already processed " + elementType + " -> Moving on...");
                continue;
            }
            processedElementTypeNames.add(elementType);

            for (Element p : injectedClasses) {
                TypeElement typeElement = (TypeElement) p;
                VariableElement variableElement = (VariableElement) element;
                if (ProcessorUtils.isSubClassOf(typeElement, elementType)) {
                    System.out.println("Mapping Injected Class [" + p + "] to Injected Type[" + elementType + "]");
                    injectedToTypeMap.put(typeElement, variableElement);
                    break;
                }
            }
        }
        if (injectedClasses.size() > injectedToTypeMap.size()) {
            System.out.println("Less Injected Classes mapped than Annotated... Must be no pairing @Inject for non-mapped.");
            for (Element p : injectedClasses) {
                TypeElement typeElement = (TypeElement) p;
                if (!injectedToTypeMap.containsKey(typeElement)) {
                    System.out.println(typeElement + " missing Inject");
                }
            }
        }

        try {
            Set<String> builtModules = new HashSet<>();
            for (Map.Entry<TypeElement, VariableElement> e : injectedToTypeMap.entrySet()) {
                TypeElement element = e.getKey();
                String moduleName = element.getAnnotation(Injected.class).value();
                builtModules.add(moduleName);

                ModuleBuilder moduleBuilder = mModuleBuilderMap.get(moduleName);
                if (moduleBuilder == null) {
                    moduleBuilder = new ModuleBuilder(moduleName);
                    mModuleBuilderMap.put(moduleName, moduleBuilder);
                }
                moduleBuilder.addMethodFor(e.getKey(), e.getValue());
            }
            // build module builder classes
            for (ModuleBuilder moduleBuilder : mModuleBuilderMap.values()) {
                moduleBuilder.build();
            }

            Set<Element> enclosingSet = new HashSet<>();
            for (Element element : elements) {
                enclosingSet.add(element.getEnclosingElement());
            }

            AnnotationValue modules = null;
            // TODO: Should this be only allowed on Application?
            Set<? extends Element> daggerComponents = roundEnvironment.getElementsAnnotatedWith(DaggerComponent.class);
            if (!daggerComponents.isEmpty()) {
                TypeElement daggerComponentAnnotation = (TypeElement) daggerComponents.iterator().next();
                for (AnnotationMirror annotationMirror : daggerComponentAnnotation.getAnnotationMirrors()) {
                    if (annotationMirror.getAnnotationType().toString().equals(DaggerComponent.class.getName())) {
                        for (Map.Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : annotationMirror.getElementValues().entrySet()) {
                            if ("modules".equals(entry.getKey().getSimpleName().toString())) {
                                modules = entry.getValue();
                                break;
                            }
                        }
                        break;
                    }
                }
            }
            // #dumbannotationprocessor
            Type[] componentModules = null;
            if (modules != null) {
                List<Attribute> componentModulesList = (List<Attribute>) modules.getValue();
                componentModules = new Type[componentModulesList.size()];
                for (int i = 0; i < componentModulesList.size(); i++) {
                    componentModules[i] = (Type) componentModulesList.get(i).getValue();
                }
            }
            ComponentBuilder componentBuilder = new ComponentBuilder(componentModules, builtModules);
            for (Element element : enclosingSet) {
                componentBuilder.addMethodFor(element);
            }
            TypeSpec componentTypeSpec = componentBuilder.build();

            new com.guivis.modulegenerator.InjectorBuilder(componentTypeSpec).build();
        } catch (ProcessorError | IOException processorError) {
            processorError.printStackTrace();
            // TODO: handle this properly
        }

        return true;
    }
}
