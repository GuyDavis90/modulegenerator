package com.guivis.modulegenerator;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.CodeBlock;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.sun.tools.javac.code.Type;

import java.io.IOException;
import java.util.List;

import javax.inject.Singleton;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.util.ElementFilter;

import dagger.Module;
import dagger.Provides;
import eu.f3rog.blade.compiler.name.NameUtils;
import eu.f3rog.blade.compiler.util.ProcessorError;
import eu.f3rog.blade.compiler.util.ProcessorUtils;

public final class ModuleBuilder {
    private static final String METHOD_NAME_PROVIDES = "provides%s";
    private static final String PACKAGE_NAME = "module";

    private String mModuleName;
    private TypeSpec.Builder mBuilder;

    public ModuleBuilder(String moduleName) throws ProcessorError {
        mModuleName = moduleName.concat("Module");
        mBuilder = TypeSpec.classBuilder(ClassName.get(PACKAGE_NAME, mModuleName).simpleName());
        mBuilder.addAnnotation(Module.class);
        mBuilder.addModifiers(Modifier.PUBLIC);
    }

    public void addMethodFor(final TypeElement typeElement, final VariableElement variableElement) throws ProcessorError {
        if (typeElement.getModifiers().contains(Modifier.ABSTRACT)) {
            return;
        }

        final Type type = ProcessorUtils.getBoundedType(variableElement);
        final TypeName typeName = ClassName.get(type);
        ClassName className = ClassName.bestGuess(typeName.toString());

        final MethodSpec.Builder forMethodBuilder = MethodSpec.methodBuilder(getMethodName(METHOD_NAME_PROVIDES, className))
                .addAnnotation(Provides.class)
                .addModifiers(Modifier.PUBLIC)
                .returns(className);

        if (typeElement.getAnnotation(Singleton.class) != null) {
            forMethodBuilder.addAnnotation(Singleton.class);
        }

        List<ExecutableElement> constructors = ElementFilter.constructorsIn(typeElement.getEnclosedElements());
        if (constructors.size() > 1) {
            System.out.println("More than one Constructor for class " + typeElement + "...");
            System.out.println("Looking for @InjectedCtor...");
            ExecutableElement injectedCtor = null;
            for (ExecutableElement constructor : constructors) {
                if (constructor.getAnnotation(InjectedCtor.class) != null) {
                    injectedCtor = constructor;
                    break;
                }
            }
            if (injectedCtor != null) {
                addReturnStatement(forMethodBuilder, typeElement, injectedCtor);
            } else {
                System.out.println("No @InjectedCtor Found!");
                throw new ProcessorError(typeElement, "Multiple Constructors but no InjectedCtor!");
            }
        } else {
            addReturnStatement(forMethodBuilder, typeElement, constructors.get(0));
        }

        mBuilder.addMethod(forMethodBuilder.build());
    }

    private MethodSpec.Builder addReturnStatement(MethodSpec.Builder builder, TypeElement typeElement, ExecutableElement constructor) {
        CodeBlock.Builder blockBuilder = CodeBlock.builder().add("return new $T(", typeElement);
        int i = 0;
        List<? extends VariableElement> parameters = constructor.getParameters();
        int numberOfParameters = parameters.size();
        for (VariableElement element : parameters) {
            final TypeName typeName = ClassName.get(element.asType());
            final String name = element.getSimpleName().toString();
            Modifier[] modifiers = new Modifier[element.getModifiers().size()];
            element.getModifiers().toArray(modifiers);
            ParameterSpec.Builder parameterBuilder = ParameterSpec.builder(typeName, name, modifiers);
            for (AnnotationMirror annotationMirror : element.getAnnotationMirrors()) {
                parameterBuilder.addAnnotation(ClassName.get((TypeElement) annotationMirror.getAnnotationType().asElement()));
            }
            ParameterSpec parameterSpec = parameterBuilder.build();
            builder.addParameter(parameterSpec);
            blockBuilder.add(name);
            if (i++ < numberOfParameters - 1) {
                blockBuilder.add(", ");
            }
        }
        blockBuilder.add(");\n");
        builder.addCode(blockBuilder.build());
        return builder;
    }

    private String getMethodName(final String format, final ClassName fragmentClassName) {
        return String.format(format, NameUtils.getNestedName(fragmentClassName, ""));
    }

    public void build() throws IOException {
        TypeSpec cls = mBuilder.build();
        // create file
        JavaFile javaFile = JavaFile.builder(PACKAGE_NAME, cls).build();
        javaFile.writeTo(ProcessorUtils.getFiler());

        System.out.println(String.format("Class <%s.%s> successfully generated.", PACKAGE_NAME, mModuleName));
    }
}
