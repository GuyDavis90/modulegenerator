package com.guivis.modulegenerator;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;

import java.io.IOException;

import javax.inject.Singleton;
import javax.lang.model.element.Modifier;

import eu.f3rog.blade.compiler.util.ProcessorError;
import eu.f3rog.blade.compiler.util.ProcessorUtils;

public final class InjectorBuilder {
    private static final String INJECTOR_NAME = "Injector";
    private static final String PACKAGE_NAME = "module";

    private TypeSpec.Builder mBuilder;

    public InjectorBuilder(TypeSpec componentType) throws ProcessorError {
        mBuilder = TypeSpec.classBuilder(INJECTOR_NAME);
        mBuilder.addModifiers(Modifier.PUBLIC);
        mBuilder.addAnnotation(Singleton.class);

        ClassName componentClassName = ClassName.bestGuess(componentType.name);
        mBuilder.addField(componentClassName, "sComponent", Modifier.PRIVATE, Modifier.STATIC);

        mBuilder.addMethod(
                MethodSpec
                        .methodBuilder("initialize")
                        .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                        .addParameter(componentClassName, "component")
                        .addStatement("sComponent = component")
                        .build()
        );

        mBuilder.addMethod(
                MethodSpec
                        .methodBuilder("get")
                        .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                        .returns(componentClassName)
                        .beginControlFlow("if (sComponent == null)")
                        .addStatement("throw new RuntimeException(\"Injector is not intialized!\")")
                        .endControlFlow()
                        .addStatement("return sComponent")
                        .build()
        );
    }

    public void build() throws IOException {
        TypeSpec cls = mBuilder.build();
        // create file
        JavaFile javaFile = JavaFile.builder(PACKAGE_NAME, cls).build();
        javaFile.writeTo(ProcessorUtils.getFiler());

        System.out.println(String.format("Class <%s.%s> successfully generated.", PACKAGE_NAME, INJECTOR_NAME));
    }
}
