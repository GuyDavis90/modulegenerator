package com.guivis.modulegenerator;

import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.sun.tools.javac.code.Type;

import java.io.IOException;
import java.util.Set;

import javax.inject.Singleton;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;

import dagger.Component;
import eu.f3rog.blade.compiler.name.NameUtils;
import eu.f3rog.blade.compiler.util.ProcessorError;
import eu.f3rog.blade.compiler.util.ProcessorUtils;

public final class ComponentBuilder {
    private static final String COMPONENT_NAME = "ApplicationComponent";
    private static final String METHOD_NAME_INJECT = "inject";
    private static final String PACKAGE_NAME = "module";

    private TypeSpec.Builder mBuilder;

    public ComponentBuilder(Type[] componentModules, Set<String> builtModules) throws ProcessorError {
        mBuilder = TypeSpec.interfaceBuilder(COMPONENT_NAME);
        mBuilder.addAnnotation(Singleton.class);
        AnnotationSpec.Builder builder = AnnotationSpec
                .builder(Component.class);
        if (componentModules != null) {
            for (Type moduleName : componentModules) {
                builder.addMember("modules", "$T.class", moduleName);
            }
        }
        for (String moduleName : builtModules) {
            // TODO: Change these Built Modules to get the type so it imports into the Component
            builder.addMember("modules", "$NModule.class", moduleName);
        }
        mBuilder.addAnnotation(builder.build());
        mBuilder.addModifiers(Modifier.PUBLIC);
    }

    public void addMethodFor(final Element element) throws ProcessorError {
        final TypeName typeName = ClassName.get(element.asType());
        ClassName className = ClassName.bestGuess(typeName.toString());

        String simpleName = className.simpleName();
        String lowercasedFirstLetter = simpleName.substring(0, 1).toLowerCase() + simpleName.substring(1);
        ParameterSpec.Builder parameterBuilder = ParameterSpec.builder(typeName, lowercasedFirstLetter);
        final MethodSpec.Builder forMethodBuilder = MethodSpec.methodBuilder(getMethodName(METHOD_NAME_INJECT, className))
                .addParameter(parameterBuilder.build())
                .addModifiers(Modifier.PUBLIC, Modifier.ABSTRACT);

        mBuilder.addMethod(forMethodBuilder.build());
    }

    private String getMethodName(final String format, final ClassName className) {
        return String.format(format, NameUtils.getNestedName(className, ""));
    }

    public TypeSpec build() throws IOException {
        TypeSpec cls = mBuilder.build();
        // create file
        JavaFile javaFile = JavaFile.builder(PACKAGE_NAME, cls).build();
        javaFile.writeTo(ProcessorUtils.getFiler());

        System.out.println(String.format("Class <%s.%s> successfully generated.", PACKAGE_NAME, COMPONENT_NAME));

        return cls;
    }
}
