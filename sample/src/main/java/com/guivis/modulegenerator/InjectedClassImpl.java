package com.guivis.modulegenerator;

import android.util.Log;


@Injected("Test")
public class InjectedClassImpl implements InjectedClass {
    private final StringProvider mStringProvider;

    public InjectedClassImpl(StringProvider stringProvider) {
        mStringProvider = stringProvider;
    }

    @Override
    public String doThing() {
        Log.i("InjectedClass", mStringProvider.getDoingString());
        return mStringProvider.getDoneString();
    }
}
