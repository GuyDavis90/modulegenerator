package com.guivis.modulegenerator;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import javax.inject.Inject;

import module.Injector;

public class MainActivity extends AppCompatActivity {
    @Inject
    InjectedClass mInjectedClass;

    @Inject
    NumberProvider mNumberProvider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Injector.get().inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((TextView) findViewById(R.id.text_view)).setText(mInjectedClass.doThing());
        ((TextView) findViewById(R.id.number_view)).setText(String.valueOf(mNumberProvider.getNumber()));
    }
}
