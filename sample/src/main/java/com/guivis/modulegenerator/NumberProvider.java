package com.guivis.modulegenerator;

import javax.inject.Singleton;


@Singleton
@Injected("Other")
public class NumberProvider extends StringProvider {
    public int getNumber() {
        return 4;
    }
}
