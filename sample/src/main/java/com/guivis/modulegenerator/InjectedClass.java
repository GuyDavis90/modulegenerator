package com.guivis.modulegenerator;

public interface InjectedClass {
    String doThing();
}
