package com.guivis.modulegenerator;

import javax.inject.Singleton;


@Singleton
@Injected("Other")
public class StringProvider {
    public String getDoingString() {
        return "Doing thing!";
    }

    public String getDoneString() {
        return "Thing done!";
    }
}
