package com.guivis.modulegenerator;

import android.app.Application;

import module.DaggerApplicationComponent;
import module.Injector;


@DaggerComponent
public class SampleApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Injector.initialize(DaggerApplicationComponent.create());
    }
}
